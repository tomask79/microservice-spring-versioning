package com.example.rest;

import com.example.rest.v1.PersonV1;
import com.example.rest.v2.PersonV2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;

@RestController
public class PersonsController {
		
    @RequestMapping(
			value = "/persons", method = RequestMethod.GET, headers = "API-Version=v1"
	)
    public Persons getPersonByIdV1() {
    	final Persons persons = new Persons();

    	final PersonV1 person = new PersonV1();
    	person.setName("Tomas");
    	person.setSurname("Kloucek");
    	person.setDepartment("Programmer");
    	persons.getPersons().add(person);
    	
    	return persons;
    }

	@RequestMapping(
			value = "/persons", method = RequestMethod.GET, headers = "API-Version=v2"
	)
	public Persons getPersonByIdV2() {
		final Persons persons = new Persons();

		final PersonV2 person = new PersonV2();
		person.setName("Tomas");
		person.setSurname("Kloucek");
		person.setDepartment("Programmer");
		// v2 new field, birth date
		Calendar calendar = Calendar.getInstance();
		calendar.set(1979, 10, 9);
		person.setBirthDate(calendar.getTime());

		persons.getPersons().add(person);

		return persons;
	}

}
