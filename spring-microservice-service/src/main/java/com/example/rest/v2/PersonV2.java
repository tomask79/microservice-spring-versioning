package com.example.rest.v2;

import com.example.rest.v1.PersonV1;

import java.util.Date;

/**
 * Created by Tomas.Kloucek on 16.1.2017.
 */
public class PersonV2 extends PersonV1 {
    public Date birthDate;

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(final Date birthDate) {
        this.birthDate = birthDate;
    }
}
