# Writing MicroServices [part 9] #

## Versioning of MicroServices ##

In this post lets talk about **versioning of MicroServices** and how you can use Spring Cloud for that. 
Well internet is full of advises like:

* Put version into service name, like **personsServicev1** or **personsServicev2**
* Put version into URL like **/persons/v1** or **/persons/v2**

Personally, **I don't like neither of them**. 

What makes sense to me is:

* To **keep the serviceNames as it is**
* Do not change service URL to have it more maintainable
* Via **custom HTTP header distinguish what version of service to call**
* Never remove or rename fields from the communications objects. Always just add new things

Let's show howto do that

Consider following two versions of data object Person:

```
package com.example.rest.v1;

import com.example.rest.Person;

public class PersonV1 implements Person {
	private String name;
	private String surname;
	private String department;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}	
}
```
and

```
package com.example.rest.v2;

import com.example.rest.v1.PersonV1;

import java.util.Date;

/**
 * Created by Tomas.Kloucek on 16.1.2017.
 */
public class PersonV2 extends PersonV1 {
    public Date birthDate;

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(final Date birthDate) {
        this.birthDate = birthDate;
    }
}
```
and following Spring REST controller:


```
package com.example.rest;

import com.example.rest.v1.PersonV1;
import com.example.rest.v2.PersonV2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;

@RestController
public class PersonsController {
		
    @RequestMapping(
			value = "/persons", method = RequestMethod.GET, headers = "API-Version=v1"
	)
    public Persons getPersonByIdV1() {
    	final Persons persons = new Persons();

    	final PersonV1 person = new PersonV1();
    	person.setName("Tomas");
    	person.setSurname("Kloucek");
    	person.setDepartment("Programmer");
    	persons.getPersons().add(person);
    	
    	return persons;
    }

	@RequestMapping(
			value = "/persons", method = RequestMethod.GET, headers = "API-Version=v2"
	)
	public Persons getPersonByIdV2() {
		final Persons persons = new Persons();

		final PersonV2 person = new PersonV2();
		person.setName("Tomas");
		person.setSurname("Kloucek");
		person.setDepartment("Programmer");
		// v2 new field, birth date
		Calendar calendar = Calendar.getInstance();
		calendar.set(1979, 10, 9);
		person.setBirthDate(calendar.getTime());

		persons.getPersons().add(person);

		return persons;
	}

}
```

To call MicroService like this you can use following code:


```
package com.example.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

@Component
public class MicroServiceClient implements CommandLineRunner {
    
    @Autowired
    private LoadBalancerClient loadBalancer;
	
	@Override
	public void run(String... arg0) throws Exception {
		final RestTemplate restTemplate = new RestTemplate();
		
		final ServiceInstance serviceInstance = loadBalancer.choose("personsService");
		if (serviceInstance != null) {
			System.out.println("Invoking instance at URL: "+serviceInstance.getUri());

			HttpHeaders headers = new HttpHeaders();
			headers.set("API-Version", arg0[0]);

			ResponseEntity<byte[]> response = restTemplate.exchange(serviceInstance.getUri()+"/persons",
					HttpMethod.GET,
					new HttpEntity<byte[]>(headers),
					byte[].class);

			System.out.println(new String(response.getBody()));
		} else {
			System.out.println("Didn't find any running instance of personsService at DiscoveryServer!");
		}
	}
}
```

Focus on the "API-Version" header.

## Testing the demo ##

Compilationing and launching of everything:

```
git clone <this repo>
mvn clean install (in the root with pom.xml)

cd spring-microservice-registry
java -jar target/registry-0.0.1-SNAPSHOT.war
verify that Eureka Server is running at http://localhost:9761

cd ..
cd spring-microservice-service
java -jar target/service-0.0.1-SNAPSHOT.war
```

Testing:

* cd ..
* cd spring-microservice-client
* java -jar target/client-0.0.1-SNAPSHOT.war **v1**

Output should be:

```
Invoking instance at URL: http://N1309.hcg.homecredit.net:8080
2017-01-17 10:51:32.877  INFO 12060 --- [erListUpdater-0] c.netflix.config.ChainedDynamicProperty  : Flipping property: personsService.ribbon.ActiveConnectionsLimit to use NEXT property: niws.loadbalancer.availabilityFilteringRule.activeConnectionsLimit = 2147483647
{"persons":[{"name":"Tomas","surname":"Kloucek","department":"Programmer"}]}
```

* cd ..
* cd spring-microservice-client
* java -jar target/client-0.0.1-SNAPSHOT.war **v2**

```
Invoking instance at URL: http://N1309.hcg.homecredit.net:8080
{"persons":[{"name":"Tomas","surname":"Kloucek","department":"Programmer","birthDate":310989212100}]}
```

cheers

Tomas