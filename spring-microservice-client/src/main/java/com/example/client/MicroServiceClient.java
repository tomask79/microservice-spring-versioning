package com.example.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

@Component
public class MicroServiceClient implements CommandLineRunner {
    
    @Autowired
    private LoadBalancerClient loadBalancer;
	
	@Override
	public void run(String... arg0) throws Exception {
		final RestTemplate restTemplate = new RestTemplate();
		
		final ServiceInstance serviceInstance = loadBalancer.choose("personsService");
		if (serviceInstance != null) {
			System.out.println("Invoking instance at URL: "+serviceInstance.getUri());

			HttpHeaders headers = new HttpHeaders();
			headers.set("API-Version", arg0[0]);

			ResponseEntity<byte[]> response = restTemplate.exchange(serviceInstance.getUri()+"/persons",
					HttpMethod.GET,
					new HttpEntity<byte[]>(headers),
					byte[].class);

			System.out.println(new String(response.getBody()));
		} else {
			System.out.println("Didn't find any running instance of personsService at DiscoveryServer!");
		}
	}
}
